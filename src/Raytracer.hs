{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Raytracer (
    generateImage, 
    Color,
    Material (..),
    Item (..),
    Scene (..),
    Light (..),
    isometricCamera
) where

import Data.Maybe
import Data.List
import qualified Data.List.Split as SP
import Data.Ord
import qualified Data.Array as A
import Control.Parallel.Strategies
import System.Random
import Control.Monad
import Control.Monad.Random
import qualified Csg
import qualified Data.Vec3 as V3
import qualified Codec.Picture as P
import Data.List.NonEmpty (NonEmpty (..)) 

type Vector = V3.CVec3

type Poly = [Vector]

type Tri = (Vector, Vector, Vector)

data Line = Line {
    origin :: Vector, 
    direction :: Vector
}

data Collision = Collision {
    collisionDistance :: Double,  
    collisionNormal :: Vector
} 
epsilon = 1e-5

generateImage :: RandomGen g => Int -> Int -> Int -> Scene -> g -> P.DynamicImage
generateImage parallelism w h scene irng = P.ImageRGB8 img
  where
    rngs = take (w*h) $ zip [1..] $ map fst $ iterate (split.snd) (split irng)
    chunkedRngs = SP.chunksOf ((w*h)`div`parallelism) rngs
    fn = (\(i, rng) -> evalRand (raytrace scene (i `mod`  w) (i `div` w)) rng)
    px = parMap rdeepseq (fn <$>) chunkedRngs 
    pxArray = A.listArray (0, w*h) ((concat px) `using` rdeepseq)
    s = fromIntegral . floor . (* 0xff)
    lookup x y = (\(r, g, b) -> P.PixelRGB8 (s r) (s g) (s b)) $ pxArray A.! (x + (y*w))
    img = P.generateImage lookup w h

data Plane = Plane {
    planeNormal :: Vector,
    planeW :: Double
} deriving Eq

triToPlane :: Tri -> Plane 
triToPlane (a, b, c) = Plane n (n V3..* a)
    where n = V3.normalize $ (b V3.<-> a) V3.>< (c V3.<-> a)

triangleCollision :: Line -> Tri -> Maybe Collision
triangleCollision line tri = r
  where
    (v0, v1, v2) = tri
    p = origin line
    d = direction line
    e1 = v1 V3.<-> v0
    e2 = v2 V3.<-> v0
    h = d V3.>< e2
    a = e1 V3..* h
    boundsCheck = abs a > epsilon
    f = 1/a
    s = p V3.<-> v0
    u = f * ( s V3..* h)
    q = s V3.>< e1
    v = f * (d V3..* q)  
    inclusionCheck = (u >= 0) && (u <= 1) && (v>=0) && (u+v <= 1) 
    t = f * (e2 V3..* q)
    directionCheck = t > epsilon
    intersects = boundsCheck && inclusionCheck && directionCheck
    coll = Collision t (V3.normalize $ e1 V3.>< e2)
    r = if intersects then Just coll else Nothing

coplanarCollision :: Line -> [Tri] -> Maybe Collision
coplanarCollision line tris = listToMaybe $ mapMaybe (triangleCollision line) tris

collision :: Line -> Csg.BspTree -> Maybe Collision
collision line (Csg.BspBranch (h:|coplanar) f b) = r
  where
    plane = triToPlane h
    dot = (direction line) V3..* (planeNormal plane)
    (front, back) = if dot <= 0 then (f, b) else (b, f)
    r = case (collision line front) of 
            Just cf -> Just cf
            Nothing -> case (coplanarCollision line (h:coplanar)) of
                        Just ccp -> Just ccp
                        Nothing -> collision line back
collision empty _ = Nothing

maybeMinimumOn :: Ord b => (a -> b) -> [a] -> Maybe a
maybeMinimumOn _ [] = Nothing
maybeMinimumOn f xs = Just $ minimumBy (comparing f) xs

--collision :: Line -> [Tri] -> Maybe Collision
--collision ray tris = closestCollision
--  where
--    maybeCollisions = map (triangleCollision ray) tris
--    validCollisions = catMaybes maybeCollisions
--    closestCollision = maybeMinimumOn collisionDistance validCollisions

randomRBG :: RandomGen gen => Rand gen (Double, Double, Double)
randomRBG = do
    r <- getRandomR (0, 0xFF)
    g <- getRandomR (0, 0xFF)
    b <- getRandomR (0, 0xFF)
    return (r, g, b)

type Color = (Double, Double, Double)
data Material = Diffuse Color | Specular Color | Emmisive Color
data Item = Item {
    itemObject :: Csg.BspTree,
    itemMaterials :: [Material]
}
data Light = PointLight Color Vector | SphereLight Color Vector Double

lightColor :: Light -> Color
lightColor (PointLight c _) = c
lightColor (SphereLight c _ _) = c

sampleLight :: RandomGen gen => Light -> Rand gen Vector
sampleLight (PointLight _ v) = return v
sampleLight (SphereLight _ v r) = ((V3.<+> v).(V3..^ r)) <$> randomUnitVector


data Scene = Scene {
    sceneItems :: [Item],
    sceneLights :: [Light],
    sceneAmbient :: Color,
    sceneInfinity :: Color,
    sceneBackground :: Color, 
    sceneCamera :: Double -> Double -> Line, 
    recursionDepth :: Int,
    initialRecursionDepth :: Int,
    sampleCount :: Int
}

isometricCamera :: Int -> Int -> Double -> Double -> Double -> Double -> Line 
isometricCamera w h planeOffset scale x y = ray
  where
    dx = scale * (x - fromIntegral w/2)  
    dy = scale * (y - fromIntegral h/2) 
    ray = Line (V3.fromXYZ (dx, -dy, planeOffset)) (V3.fromXYZ (0, 0, -1))

extractFstMaybe :: (Maybe a, b) -> Maybe (a, b)
extractFstMaybe (Just c, m) = Just (c, m)
extractFstMaybe (Nothing, _) = Nothing

sceneCollision :: Line -> Scene -> Maybe (Collision, [Material])
sceneCollision ray scene = closestCollision
  where
    collisions = map (collision ray . itemObject) (sceneItems scene)
    collisionMaterials = mapMaybe extractFstMaybe $ zip collisions (map itemMaterials (sceneItems scene))
    closestCollision = maybeMinimumOn (collisionDistance . fst) collisionMaterials

addColors :: Color -> Color -> Color
addColors (r1, g1, b1) (r2, g2, b2) = (r1 + r2, g1 + g2, b1 + b2)

sumColors :: [Color] -> Color
sumColors = foldl addColors (0, 0, 0)

multiplyColors :: Color -> Color -> Color
multiplyColors (r1, g1, b1) (r2, g2, b2) = (r1*r2, g1*g2, b1* b2)

scaleColor :: Double -> Color -> Color
scaleColor s (r, g, b) = (s*r, s*g, s*b)

specularRay :: Vector -> Vector -> Vector
specularRay normal ray = ray V3.<-> (normal V3..^((normal V3..* ray) * 2))

randomUnitVector :: RandomGen gen => Rand gen Vector
randomUnitVector = do
    x <- getRandomR (-1, 1)
    y <- getRandomR (-1, 1)
    z <- getRandomR (-1, 1)
    let v = V3.fromXYZ (x, y, z)
    if V3.norm v < 1.0 then return $ V3.normalize v else randomUnitVector

collisionPoint :: Line -> Collision -> Vector
collisionPoint ray col = (origin ray V3.<+> ((direction ray) V3..^ (collisionDistance col))) 

diffuseRay :: RandomGen gen => Vector -> Rand gen Vector
diffuseRay normal = do
    uv <- randomUnitVector
    return $ V3.normalize (normal V3.<+> uv) 

diffuseLightColor :: RandomGen gen => Scene -> Line ->  Collision -> Light -> Rand gen Color
diffuseLightColor scene inboundRay inboundCollision light = do 
    let o = collisionPoint inboundRay inboundCollision
    lightPoint <- sampleLight light
    let toLight = lightPoint V3.<-> o
    let outboundDirection = V3.normalize toLight
    let outboundRay = Line { origin=o, direction=outboundDirection }
    let lightDistance = V3.norm toLight
    let reachedLight = case sceneCollision outboundRay scene of 
                        Nothing -> True
                        Just (col,_) -> if collisionDistance col > lightDistance then True else False
    let angleScale = max 0 (collisionNormal inboundCollision V3..* outboundDirection)
    let distanceScale = 1 / (lightDistance * lightDistance) 
    return $ if reachedLight then scaleColor (angleScale * distanceScale) $ lightColor light else (0,0,0)

diffuseLightsColor :: RandomGen gen => Scene -> Line -> Collision -> Rand gen Color
diffuseLightsColor scene ray col = sumColors <$> sequence ( (diffuseLightColor scene ray col) <$> sceneLights scene )

recurseMaterial :: RandomGen gen => Scene -> Line -> Collision -> Material -> Rand gen Color
recurseMaterial _ _ _ (Emmisive c) = return c

recurseMaterial scene ray col (Specular c) = do
    let reflection = specularRay (collisionNormal col) (direction ray)
    let nextRay = Line { origin=collisionPoint ray col, direction=reflection }
    let nextDepth = recursionDepth scene - 1
    recursedColor <- recursiveRaytrace (scene {recursionDepth=nextDepth}) nextRay
    return $ multiplyColors c recursedColor

recurseMaterial scene ray col (Diffuse c) = do
    reflection <- diffuseRay (collisionNormal col)
    let nextRay = Line { origin = collisionPoint ray col, direction=reflection }
    let nextDepth = recursionDepth scene - 1
    recursedColor <- recursiveRaytrace (scene {recursionDepth= nextDepth}) nextRay
    lightColor <- diffuseLightsColor scene ray col
    return $ multiplyColors c (recursedColor `addColors` lightColor)

recursiveRaytrace :: RandomGen gen => Scene -> Line -> Rand gen Color
recursiveRaytrace Scene { sceneAmbient=sa,  recursionDepth = 0 } _ = return sa
recursiveRaytrace scene ray = 
    case sceneCollision ray scene of 
        Just (col, mat) -> sumColors <$> mapM (recurseMaterial scene ray col) mat

        Nothing -> return $ atInfinity scene
          where 
            atInfinity s = (if recursionDepth s == initialRecursionDepth s then sceneBackground else sceneInfinity) s

randomRaytrace :: RandomGen gen => Scene -> Int -> Int -> Rand gen Color
randomRaytrace scene x y = do
    dx <- getRandomR (-0.5, 0.5)
    dy <- getRandomR (-0.5, 0.5)
    let ray = sceneCamera scene (dx + fromIntegral x) (dy + fromIntegral y)
    recursiveRaytrace scene ray

raytrace :: RandomGen g => Scene -> Int -> Int -> Rand g Color
raytrace scene x y = do
    let samples = sampleCount scene
    colors <- replicateM samples $ interleave $ randomRaytrace scene x y
    let (r, g, b) = sumColors colors
    let s = (min 1) . (/ fromIntegral samples)
    return (s r, s g, s b) 
    
